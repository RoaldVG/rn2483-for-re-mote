//rn2483_radio_rxbuf//
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>
#include "os/dev/radio.h"
#include "os/dev/leds.h"
#include "os/sys/clock.h"
#include "os/sys/ctimer.h"
#include "os/sys/rtimer.h"
#include "dev/uart.h"
#include "net/mac/tsch/tsch.h"
#include "gpio.h"
//#include "uart.h"

#include "contiki.h"
#include "os/net/packetbuf.h"
//#include "net/rime/rimestats.h"
#include "os/net/netstack.h"
#include <inttypes.h>
//#include "platform/loramote/LoRaMac/mac/LoRaMac.h"

#include "rn2483.h"
#include "rn2483-config.h"
#include "rn2483-uart.h"
#include "net/mac/framer/frame802154.h"
/*---------------------------------------------------------------------------*/
#define DEBUG 1
#if DEBUG
  #define PRINTF(...)	printf(__VA_ARGS__)
#else
  #define PRINTF(...)
#endif
/*---------------------------------------------------------------------------*/
//#define CLEAR_RXBUF()           (rn2483_radio_rxbuf[0] = 0)
//#define IS_RXBUF_EMPTY()        (rn2483_radio_rxbuf[0] == 0)

/*------------------------------------------------------------------*/
/* Frame cannot be parsed / is to short */
#define INVALID_FRAME                   0
/* Address check failed */
#define ADDR_CHECK_FAILED               1
/* Address check succeeded */
#define ADDR_CHECK_OK                   2
/* Address check succeeded and ACK was send */
#define ADDR_CHECK_OK_ACK_SEND          3
/*---------------------------------------------*/
#define LORA_RADIO_MAC 0
/*------------------------ADDED-------------------------------------*/
/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "LoRa"
#define LOG_LEVEL LOG_LEVEL_MAC


/* Current radio channel */
 int current_channel = RF_FREQUENCY;

/* We are currently in poll mode? Disabled by default */
static uint8_t volatile poll_mode = 0;

/* Do we perform a CCA before sending? Disabled by default. */
//static uint8_t send_on_cca = 1;

/* SFD timestamp in RTIMER ticks */
static volatile uint32_t last_packet_timestamp = 0;

/* Is the driver currently transmitting a software ACK? */
//static uint8_t in_ack_transmission = 0;


/*---------------------------------------------------------------------------*/
/* Incoming data buffer, the first byte will contain the length of the packet */
//static uint8_t rn2483_radio_rxbuf[LORA_MAX_PAYLOAD_SIZE + 1];
//static RadioEvents_t RadioEvents;
static int packet_is_prepared = 0;
static const void *packet_payload;
static unsigned short packet_payload_len = 0;
static packetbuf_attr_t last_rssi = 0;

//static radio_value_t rx_mode_value;
uint8_t lora_snr = 0;
/*---------------------------------------------------------------------------*/
//static packetbuf_attr_t channel = RF_FREQUENCY;
//static uint8_t snr = 0;
//static int last_rssi = 0;

/* Added */
#define RF_CHANNEL_MIN	868000000 //21
#define RF_CHANNEL_SPACING 200000 //1


/* Autoack */
//#ifndef MICROMAC_CONF_AUTOACK
#define MICROMAC_CONF_AUTOACK 1
//#endif /* MICROMAC_CONF_AUTOACK */

/* (Software) frame filtering enabled by default */
//static uint8_t frame_filtering = 1;
/* (Software) autoack */
//static uint8_t autoack_enabled = 1; //MICROMAC_CONF_AUTOACK;
//static uint8_t autoack_enabled = 1;
//LoRaMacFrameCtrl_t FrameCtrl;
//MibRequestConfirm_t *mibGet;
/***************************************/

static char rxbuf_data[64]={0};
#define INTER_PACKET_INTERVAL  RTIMER_ARCH_SECOND/ 10 //=100ms
#define CLEAR_RXBUF()		(rxbuf_data[0] = 0)
#define IS_RXBUF_EMPTY()	(rxbuf_data[0] == 0)
static struct ctimer timer;
//static int i=0;
static int u=10;
static const void *packet_payload;
//static unsigned short packet_payload_len=0;
//static int start_channel = 0;
bool radio_on_state = 1;

char *system_enum[3]={"ver","vdd","hweui"};
/*---------------------------------------------------------------------------*/
void blink_led();
static void BRScallback(void *ptr);// get Brief Radio Settings callback
static void RScallback(void *ptr);// Radio Settings callback
void vset_settings(const char *setting, const char *first, va_list argp);
void vget_settings(const char *setting, const char *first, va_list argp);
static int rn2483_init(void);
static int rn2483_prepare(const void *payload, unsigned short payload_len);
static int rn2483_transmit(unsigned short payload_len);
static int rn2483_radio_send(const void *payload, unsigned short payload_len);
static int rn2483_radio_read(void *buf, unsigned short bufsize);
static int rn2483_channel_clear(void);
static int rn2483_receiving_packet(void);
static int rn2483_pending_packet(void);
static int rn2483_off(void);
static int rn2483_on(void);
//static void send_ack(const frame802154_t *frame);
//static int is_packet_for_us(uint8_t *buf, int len );
//static int check_auto_ack(uint8_t *buf, int buflen);
//static int software_auto_ack(uint8_t *buf, int buflen);
/*---------------------------------------------------------------------------*/
//RadioModems_t modem;
//void OnCadDone(bool channelActivityDetected);
int i ;
char buf[32];
int buffRead[256];
/*-----------------------------------------------------*/
int radio_flag=0;

/* TSCH timeslot timing (mircoseconds) */
/* static const tsch_timeslot_timing_usec rn2483_tsch_timing = {
  RN2483_TSCH_DEFAULT_TS_CCA_OFFSET,
  RN2483_TSCH_DEFAULT_TS_CCA,
  RN2483_TSCH_DEFAULT_TS_TX_OFFSET,
  RN2483_TSCH_DEFAULT_TS_RX_OFFSET,
  RN2483_TSCH_DEFAULT_TS_RX_ACK_DELAY,
  RN2483_TSCH_DEFAULT_TS_TX_ACK_DELAY,
  RN2483_TSCH_DEFAULT_TS_RX_WAIT,
  RN2483_TSCH_DEFAULT_TS_ACK_WAIT,
  RN2483_TSCH_DEFAULT_TS_RX_TX,
  RN2483_TSCH_DEFAULT_TS_MAX_ACK,
  RN2483_TSCH_DEFAULT_TS_MAX_TX,
  RN2483_TSCH_DEFAULT_TS_TIMESLOT_LENGTH,
};*/

//RadioModems_t modem = MODEM_LORA;
uint8_t pktLen = (uint8_t)(LORA_MAX_PAYLOAD_SIZE);
//static rtimer_clock_t t0;
static radio_result_t rn2483_get_value(radio_param_t param, radio_value_t *value);
static radio_result_t rn2483_set_value(radio_param_t param, radio_value_t value);
static radio_result_t rn2483_get_object(radio_param_t param, void *dest, size_t size);
static radio_result_t rn2483_set_object(radio_param_t param, const void *src, size_t size);
//static rtimer_clock_t get_packet_timestamp(void);
/*=================== END ADDING ==========================*/
/*---------------------------------------------------------------------------*/
PROCESS_NAME(rn2483_process);
PROCESS(rn2483_process,"RN2483 driver process");
/*---------------------------------------------------------------------------*/
const struct radio_driver rn2483_radio_driver =
{
  rn2483_init,
  rn2483_prepare,
  rn2483_transmit,
  rn2483_radio_send,
  rn2483_radio_read,
  rn2483_channel_clear,
  rn2483_receiving_packet,
  rn2483_pending_packet,
  rn2483_off,
  rn2483_on,
	rn2483_get_value,
	rn2483_set_value,
	rn2483_get_object,
	rn2483_set_object
};
/*---------------------------------------------------------------------------*/
/*=======================string and parsing functions========================*/
void my_reverse(char str[], int len)
{
    int start, end;
    char temp;
    for(start=0, end=len-1; start < end; start++, end--) {
        temp = *(str+start);
        *(str+start) = *(str+end);
        *(str+end) = temp;
    }
}
 /*---------------------------------------------------------------------------*/
char* my_itoa(int num, char* str, int base)
{
    int i = 0;
    bool isNegative = false;

    /* A zero is same "0" string in all base */
    if (num == 0) {
        str[i] = '0';
        str[i + 1] = '\0';
        return str;
    }

    /* negative numbers are only handled if base is 10
       otherwise considered unsigned number */
    if (num < 0 && base == 10) {
        isNegative = true;
        num = -num;
    }

    while (num != 0) {
        int rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'A' : rem + '0';
        num = num/base;
    }

    /* Append negative sign for negative numbers */
    if (isNegative){
        str[i++] = '-';
    }

    str[i] = '\0';

    my_reverse(str, i);

    return str;
}
/*---------------------------------------------------------------------------*/
char* bw_translate(int bw)
{
  switch(bw)
  {
    case 0: return "125";
    case 1: return "250";
    case 2: return "500";
    default: return 0;
  }
}
/*---------------------------------------------------------------------------*/
char* cr_translate(int cr)
{
  switch(cr)
  {
    case 1: return "4/5";
    case 2: return "4/6";
    case 3: return "4/7";
    case 4: return "4/8";
    default: return 0;
  }
}
/*---------------------------------------------------------------------------*/
/*==========================Radio driver functions===========================*/
static int rn2483_init(void)
{
  //printf("initializing..\n");
  lora_reception_init();

  /*
  RadioEvents.TxDone = OnTxDone;
  RadioEvents.RxDone = OnRxDone;
  RadioEvents.TxTimeout = OnTxTimeout;
  RadioEvents.RxTimeout = OnRxTimeout;
  RadioEvents.RxError = OnRxError;
  Radio.Init(&RadioEvents);
  Radio.SetChannel(RF_FREQUENCY);
  */

  //rn2483_off();
  //Ldelay();
  rn2483_on();
  Ldelay();
  rn2483_send_cmd("sys","reset");
  Ldelay();
  Ldelay();
  rn2483_send_cmd("mac set","class a");
  Ldelay();
  rn2483_send_cmd("mac","pause");
  Ldelay();

  #if USE_LORA_MODEM
    printf("rn2483: USING LORA MODEM\n");
    rn2483_send_cmd("radio set mod","lora");
    Ldelay();
    rn2483_send_cmd("radio set freq", my_itoa(RF_FREQUENCY*1000000,buf,10));
    Ldelay();
    sprintf(buf,"%d",TX_OUTPUT_POWER);
    rn2483_send_cmd("radio set pwr", buf);
    Ldelay();
    rn2483_send_cmd("radio set bw", bw_translate(LORA_BANDWIDTH));
    Ldelay();
    sprintf(buf,"sf%d",LORA_SPREADING_FACTOR);
    rn2483_send_cmd("radio set sf", buf);
    Ldelay();
    rn2483_send_cmd("radio set cr", cr_translate(LORA_CODINGRATE));
    Ldelay();
    rn2483_send_cmd("radio set prlen", my_itoa(LORA_PREAMBLE_LENGTH,buf,10));
    Ldelay();
    rn2483_send_cmd("radio set crc", LORA_CRC_ON ? "on" : "off");
    Ldelay();
    rn2483_send_cmd("radio set iqi", LORA_IQ_INVERSION_ON ? "on" : "off");
    Ldelay();
    rn2483_send_cmd("radio set wdt", "2000");//my_itoa(TX_TIMEOUT_VALUE,buf,10));
    Ldelay();

    /*set_radio_settings("mod", USE_LORA_MODEM ? "lora" : "fsk",
      "freq", my_itoa(RF_FREQUENCY*1000000,buf,10),
      "pwr", my_itoa(TX_OUTPUT_POWER,buf,10),
      "bw", bw_translate(LORA_BANDWIDTH),
      "sf", my_itoa(LORA_SPREADING_FACTOR,buf,10),
      "cr", cr_translate(LORA_CODINGRATE),
      "prlen", my_itoa(LORA_PREAMBLE_LENGTH,buf,10),
      "crc", LORA_CRC_ON ? "on" : "off",
      "iqi", LORA_IQ_INVERSION_ON ? "on" : "off",
      "wdt", my_itoa(TX_TIMEOUT_VALUE,buf,10));*/
  #else
    printf("rn2483: USING FSK MODEM\n");
    rn2483_send_cmd("mod","fsk");
    rn2483_send_cmd("freq", my_itoa(RF_FREQUENCY*1000000,buf,10));
    rn2483_send_cmd("pwr", my_itoa(TX_OUTPUT_POWER,buf,10));
    rn2483_send_cmd("bw", my_itoa(FSK_BANDWIDTH,buf,10));
    rn2483_send_cmd("fdev", my_itoa(FSK_FDEV,buf,10));
    rn2483_send_cmd("bitrate", my_itoa(FSK_DATARATE,buf,10));
    rn2483_send_cmd("prlen", my_itoa(FSK_PREAMBLE_LENGTH,buf,10));
    rn2483_send_cmd("crc", FSK_CRC_ON ? "on" : "off");
    rn2483_send_cmd("wdt", my_itoa(TX_TIMEOUT_VALUE,buf,10));
    rn2483_send_cmd("afcbw", FSK_AFC_BANDWIDTH/1000);
    /*set_radio_settings("mod", !USE_LORA_MODEM? "fsk" : "lora",
    "freq", my_itoa(RF_FREQUENCY*1000000,buf,10),
    "pwr", my_itoa(TX_OUTPUT_POWER,buf,10),
    "bw", my_itoa(FSK_BANDWIDTH,buf,10),
    "fdev", my_itoa(FSK_FDEV,buf,10),
    "bitrate", my_itoa(FSK_DATARATE,buf,10),
    "prlen", my_itoa(FSK_PREAMBLE_LENGTH,buf,10),
    "crc", FSK_CRC_ON ? "on" : "off",
    "wdt", my_itoa(TX_TIMEOUT_VALUE,buf,10),
    "afcbw", FSK_AFC_BANDWIDTH/1000);*/
    //#error "Please define a modem in the compiler options."
  #endif

  #if RX_CONTINUOUS_MODE
    rn2483_send_cmd("mac","pause");
    Ldelay();
    rn2483_send_cmd("radio rx", "0");//busy
  #endif

  //rn2483_send_cmd("mac","resume");

  process_start(&rn2483_process,NULL);

  printf("rn2483: RADIO INITIALISED\n");
  return 1;
}
/*---------------------------------------------------------------------------*/
/* Check broadcast address. */
/*static int
is_broadcast_addr(uint8_t mode, uint8_t *addr)
{
  int i = ((mode == FRAME802154_SHORTADDRMODE) ? 2 : 8);
  while(i-- > 0) {
    if(addr[i] != 0xff) {
      return 0;
    }
  }
  return 1;
}*/
/*---------------------------------------------------------------------------*/
/*static void
send_ack(const frame802154_t *frame)
{

	uint8_t buffer[3];
	  // FCF: 2 octets
	  buffer[0] = FRAME802154_ACKFRAME;
	  buffer[1] = 0;
	  // Seqnum: 1 octets
	  buffer[2] = frame->seq;
	  //printf("Line 312 value of frame sequence is %d\n\n",frame->seq);
	  in_ack_transmission = 1;
	  ptp_send((char *)buffer, sizeof(buffer));
	  in_ack_transmission = 0;
}*/
static int rn2483_prepare(const void *payload, unsigned short payload_len)
{
  packet_is_prepared = 0;
  // Checks if the payload length is supported
  if(payload_len > LORA_MAX_PAYLOAD_SIZE ) {
    return RADIO_TX_ERR;
  }

  packet_payload = payload;
  packet_payload_len = payload_len;
  packet_is_prepared = 1;
  return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int rn2483_transmit(unsigned short payload_len)
{
  bool was_off = 0;

  if(!packet_is_prepared) {
    return RADIO_TX_ERR;
  }

  rn2483_is_transmitting = 1;
  //PRINTF("RADIO TX packet with len %d\n", packet_payload_len);

  if(!radio_on_state) // Radio is off - turn it on
  {
    was_off = 1;
    rn2483_on();
  }

  ptp_send((uint8_t *)packet_payload, sizeof(packet_payload));
  //rn2483_send_cmd("radio tx", my_itoa((uint)packet_payload,buf,16));

  packet_is_prepared = 0;
  
  if(was_off)
    rn2483_off();
  return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int rn2483_radio_send(const void *payload, unsigned short payload_len)//point-to-point link: hex value; 48656c6c6f = hello /
{
  if(rn2483_is_transmitting){
    return RADIO_TX_COLLISION;
  }
  if(rn2483_prepare(payload, payload_len) == RADIO_TX_ERR) {
  return RADIO_TX_ERR;
  }
  return rn2483_transmit(payload_len);
}
/*---------------------------------------------------------------------------*/
static int rn2483_radio_read(void *buf, unsigned short bufsize)
{
  if(IS_RXBUF_EMPTY()){
    printf("READ: RX BUFFER EMPTY\n");
    return 0;
  }
  if(bufsize<64){
    printf("READ: BUFFER TOO SMALL\n");
    printf("READ: DROPPING PACKET\n");
		CLEAR_RXBUF();
    return 0;
  }

  memcpy(buf, rxbuf_data+1, rxbuf_data[0]);
  packetbuf_set_attr(PACKETBUF_ATTR_RSSI, last_rssi);
  bufsize = rxbuf_data[0];

  CLEAR_RXBUF();
  return 1;
}
/*---------------------------------------------------------------------------*/
static int rn2483_channel_clear(void)
{
  return 0;
}
/*---------------------------------------------------------------------------*/
static int rn2483_receiving_packet(void)
{
  return 0;
}
/*---------------------------------------------------------------------------*/
static int rn2483_pending_packet(void)
{
  //printf("pending packet\n");
  return !IS_RXBUF_EMPTY();
}
/*---------------------------------------------------------------------------*/
static int rn2483_off(void)
{
  if (radio_on_state)
  {
    Ldelay();
    //printf("RADIO OFF\n");
    //RN2483_sleep(15000);
    radio_on_state = 0;
    //Ldelay();
  }
  else
    printf("Radio already off\n");
  return 1;
}
/*---------------------------------------------------------------------------*/
static int rn2483_on(void)
{
  if (!radio_on_state)
  {
    //printf("RADIO ON\n");
    /*REG(UART_1_BASE+UART_LCRH) |= UART_LCRH_BRK;
    clock_delay_usec(750);
    REG(UART_1_BASE+UART_LCRH) &= ~UART_LCRH_BRK;
    uart_write_byte(1,0x55);*/
    GPIO_CLR_PIN(GPIO_D_BASE,GPIO_PIN_MASK(3));
    clock_delay_usec(50);
    GPIO_SET_PIN(GPIO_D_BASE,GPIO_PIN_MASK(3));
    radio_on_state = 1;
    Ldelay();
    Ldelay();
  }
  else
    printf("Radio already on\n");
  return 1;
}
/*---------------------------------------------------------------------------*/
int32_t set_channel(int channel)
{
  int freq;
  bool was_off;

  if(!radio_on_state)
  {
    was_off = 1;
    rn2483_on();
  }

  switch(LORA_BANDWIDTH){
    case 0:
      switch(channel)
      {
        case 20: freq = 868100000;
        case 21: freq = 868300000;
        case 22: freq = 868500000;
        case 23: freq = 868866667;
        case 24: freq = 869033333;
        default: return 0;
      };
    case 1:
      switch(channel)
      {
        case 20: freq = 868300000;
        case 21: freq = 868950000;
        default: return 0;
      }
  }
  set_radio_settings("freq", freq);
  if (was_off)
    rn2483_off();
  return freq;
}
/*---------------------------------------------------------------------------*/
static radio_result_t rn2483_get_value(radio_param_t param, radio_value_t *value)
{
  return 0;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
rn2483_set_value(radio_param_t param, radio_value_t value)
{
  return 0;
}
/*---------------------------------------------------------------------------*/
static radio_result_t
rn2483_get_object(radio_param_t param, void *dest, size_t size)
{
  if(param == RADIO_PARAM_LAST_PACKET_TIMESTAMP) {
    if(size != sizeof(rtimer_clock_t) || !dest) {
      return RADIO_RESULT_INVALID_VALUE;
    }
    *(rtimer_clock_t *)dest = RTIMER_NOW();//TimerGetCurrentTime();//
    return RADIO_RESULT_OK;
  }

#if MAC_CONF_WITH_TSCH
  if(param == RADIO_CONST_TSCH_TIMING) {
    if(size != sizeof(uint16_t *) || !dest) {
      return RADIO_RESULT_INVALID_VALUE;
    }
    /* Assigned value: a pointer to the TSCH timing in usec */
    *(const uint32_t **)dest = rn2483_tsch_timing;
    return RADIO_RESULT_OK;
  }
#endif /* MAC_CONF_WITH_TSCH */

  return RADIO_RESULT_NOT_SUPPORTED;
}

/*--------------------------------------------------------------------------*/
static radio_result_t
rn2483_set_object(radio_param_t param, const void *src, size_t size)
{
  return RADIO_RESULT_NOT_SUPPORTED;
}
/*---------------------------------------------------------------------------*/
/*==============================RN2483 commands==============================*/
/*
 *system commands
 */
/*---------------------------------------------------------------------------*/
void RN2483_version()
{
   rn2483_send_cmd("sys get ver", " ");
   blink_led();
}
/*---------------------------------------------------------------------------*/
void RN2483_sleep(int duration)// #miliseconds the system will sleep
{
   char cduration[11];
   my_itoa(duration,cduration,10);
   //printf("sleeping %s ms\n", cduration);
   rn2483_send_cmd("sys sleep", cduration);
   blink_led();
}
/*---------------------------------------------------------------------------*/
void RN2483_reset()
{
  rn2483_send_cmd("sys reset", " ");
  blink_led();
}
/*---------------------------------------------------------------------------*/
void RN2483_Freset()
{
  rn2483_send_cmd("sys factoryRESET", " ");
  blink_led();
}

/*---------------------------------------------------------------------------*/
void RN2483_StoreData(char* address,char* data) // address from 300 to 3FF, data from 00 to FF, chars because the values have to be entered in a hexadecimal values
{
   char c[strlen(address)+strlen(data)+14];
   strcpy(c,address);
   strcat(c," ");
   strcat(c,data);
   printf("data stored in EEPROM ...\n");
   rn2483_send_cmd("sys set nvm", c);
   blink_led();
}
/*---------------------------------------------------------------------------*/
int get_system_settings(char *setting)
{
  for(i=0;i<3;i++){
  	if(!strcmp(system_enum[i],setting)){
  	u=i;
  	}
  }
  switch(u) {
   case 0 :
      printf("version: \r\n");
      rn2483_send_cmd("sys get ver", " ");
      break;
   case 1 :
      printf("VDD (mV): \r\n");
      rn2483_send_cmd("sys get vdd", " ");
      break;
   case 2 :
      printf("programmed EUI node address: \r\n");
      rn2483_send_cmd("sys get hweui", " ");
      break;
   default :
      printf("error: wrong argument\r\n");
      break;
	}
  u=10;
  i=0;
  blink_led();
  return 1;
}
/*---------------------------------------------------------------------------*/
void RN2483_GetData(char* address)
{
   rn2483_send_cmd("sys get nvm", address);
   blink_led();
}
/*---------------------------------------------------------------------------*/
/*
 * MAC commands
 */
/*---------------------------------------------------------------------------*/
void mac_reset(int band)
{
   char cband[4];
   my_itoa(band,cband,10);
   printf("reset ... : %d\n", band);
   rn2483_send_cmd("mac reset", cband);
   blink_led();
}
/*---------------------------------------------------------------------------*/
void mac_send(char *type,int portnumber,char *data)
{
   char c[strlen(type)+4+strlen(data)];
   char cportnumber[4];
   my_itoa(portnumber,cportnumber,10);
   strcpy(c,type);
   strcat(c," ");
   strcat(c,cportnumber);
   strcat(c," ");
   strcat(c,data);
   //printf("sending ... : %s", c);
   rn2483_send_cmd("mac tx", c);
   blink_led();
}
/*---------------------------------------------------------------------------*/
void mac_join(char *mode)// otaa or abp // called by join OTAA or ABP
{
  if(!strcmp(mode,"otaa")){
	   printf("joining network using otaa procedure: ... \n");
     rn2483_send_cmd("mac join otaa", " ");
 }
 else if(!strcmp(mode,"abp")){
	  printf("joining network using abp procedure: ... \n");
    rn2483_send_cmd("mac join abp", " ");
 }
 else{printf("error: not a valid mode\n");}
 blink_led();
}
/*---------------------------------------------------------------------------*/
void mac_save()
{
   printf("Saving LoRaWAN Class A configuration parameters to EEPROM:... \n");
   rn2483_send_cmd("mac save", " ");
   blink_led();
}
/*---------------------------------------------------------------------------*/
void mac_force_enable()
{
   printf("Forcing enable: ... \r\n");
   rn2483_send_cmd("mac forceENABLE", " ");
   blink_led();
}
/*---------------------------------------------------------------------------*/
int set_mac_settings(const char *first, ...)
{
	va_list argp;
	va_start(argp,first);
	vset_settings("mac set",first,argp);
	va_end(argp);
	return 1;
}
/*---------------------------------------------------------------------------*/
int set_mac_channel_settings(const char *first, ...)
{
	va_list argp;
	va_start(argp,first);
	vset_settings("mac set ch",first,argp);
	va_end(argp);
	return 1;
}
/*---------------------------------------------------------------------------*/
int get_mac_settings(const char *first, ...)
{
	static const char setting[]="mac get";
	va_list argp;
	va_start(argp,first);
	vget_settings(setting,first,argp);
	va_end(argp);
	return 1;
}
/*---------------------------------------------------------------------------*/
int get_mac_channel_settings(const char *first, ...)
{
	va_list argp;
	va_start(argp,first);
	vset_settings("mac get ch",first,argp);
	va_end(argp);
	return 1;
}
/*---------------------------------------------------------------------------*/
/*
 * radio commands
 */
/*---------------------------------------------------------------------------*/
void radio_settings()
{
  ctimer_set(&timer, CLOCK_SECOND/20, BRScallback,NULL);//to print all the settings, it will take 6/20 seconds (0.3s)
}
/*---------------------------------------------------------------------------*/
void macpause()
{
  rn2483_send_cmd("mac pause", " ");
}
/*---------------------------------------------------------------------------*/
void power(char *power)
{
  rn2483_send_cmd("radio set pwr", power);
	blink_led();
}
/*---------------------------------------------------------------------------*/
void ptp_send(uint8_t *message, uint8_t size )//point-to-point link: hex value; 48656c6c6f = hello /
{
  char* c;
  c = malloc(size+1);
  for(int i=0;i<size;i++)
  {
    i==0 ? strcpy(c,my_itoa(message[i],buf,16)) : strcat(c,my_itoa(message[i],buf,16));
    //rn2483_send_cmd("radio tx", my_itoa(message[i],buf,16));
  }
  //printf("sending message: %s\n", c);
  rn2483_send_cmd("radio tx", c);
	blink_led();
}
/*---------------------------------------------------------------------------*/
void ptp_receive(int window)//point-to-point link
{
   char cwindow[20];
   my_itoa(window,cwindow,10);
   printf("waiting for reception...\n");
   rn2483_send_cmd("radio rx", cwindow);
}
/*---------------------------------------------------------------------------*/
/*static void
ptpcallback(char ptr[24])
{
	if(i==0){
	printf("mac pause: \r\n");
	lora_send("mac pause\r\n");
	i++;
	ctimer_reset(&timer);
	}
	if(i==1){
	printf("%s",ptr[i-1]);
	lora_send(ptr[i-1]);
	i++;
	ctimer_reset(&timer);
	}
	else{
	printf("mac resume: ");
	lora_send("mac resume\r\n");
	i=0;
	blink_led();
	}
}*/
/*---------------------------------------------------------------------------*/
void cw_state(char *state)//continous wave state
{
   printf("pause mac: ...\r\n");
   printf("changing cw state: ...\r\n");
   rn2483_send_cmd("mac pause", " ");
   Ldelay();
   if(!strcmp(state,"on")){rn2483_send_cmd("radio cw on", " ");}
   else if(!strcmp(state,"off")) {rn2483_send_cmd("radio cw off", " ");}
   else{printf("error: not a valid state\r\n");}
   Ldelay();
   rn2483_send_cmd("mac resume", " ");
}
/*---------------------------------------------------------------------------*/
int set_radio_settings(const char *first, ...)
{
	va_list argp;
	va_start(argp,first);
	vset_settings("radio set",first,argp);
	va_end(argp);
	return 1;
}
/*---------------------------------------------------------------------------*/
int get_radio_settings(const char *first, ...)
{
	va_list argp;
	va_start(argp,first);
	vget_settings("radio get",first,argp);
	va_end(argp);
	return 1;
}
/*---------------------------------------------------------------------------*/
/*
* radio callback functions
*/
/*---------------------------------------------------------------------------*/
static void
BRScallback(void *ptr)
{
   switch(i) {
   case 0 :
      printf("Radio transmission settings:\n");
      printf("pause duration of the mac level: \n");
      rn2483_send_cmd("mac pause", " ");// pause the mac level
      i++;
      ctimer_reset(&timer);
      break;
   case 1 :
      printf("mode: ");
      rn2483_send_cmd("radio get mod", " ");// get the used modulation technique
      i++;
      ctimer_reset(&timer);
      break;
   case 2 :
      printf("frequency (Hz): ");
      rn2483_send_cmd("radio get freq", " ");// get the used frequency in Hertz
      i++;
      ctimer_reset(&timer);
      break;
   case 3 :
      printf("spread factor: ");
      rn2483_send_cmd("radio get sf", " ");// get the spread factor
//(increase sensitivity of the reception, increase duration of transmission) SNR can be up to -20dB
      i++;
      ctimer_reset(&timer);
      break;
  	   case 4 :
      printf("bandwidth (KHz): ");
      rn2483_send_cmd("radio get bw", " ");// get the bandwidth
      i++;
      ctimer_reset(&timer);
      break;
   case 5 :
      printf("transmission power (dB): ");
      rn2483_send_cmd("radio get pwr", " ");// get the transmission power
      i++;
      ctimer_reset(&timer);
      break;
   case 6 :
      printf("resume mac: ");
      rn2483_send_cmd("mac resume", " ");
      i=0;
      blink_led();
      break;
  }
}
/*---------------------------------------------------------------------------*/
static void
RScallback(void *ptr)
{
	if(i==0){
  	printf("mac pause: \r\n");
  	rn2483_send_cmd("mac pause", " ");
  	i++;
  	ctimer_reset(&timer);
	}
	else{
		if(strcmp((char*)(ptr+(i-1)*20*2*71),"STOP")>0){
  		printf("%s\n",(char*)(ptr+(i-1)*20*2*71));
  		rn2483_send_cmd((char*)(ptr+(i-1)*20*2*71), " ");
  		i++;
  		ctimer_reset(&timer);
		}
		else{
  		printf("mac resume: ");
  		rn2483_send_cmd("mac resume", " ");
  		i=0;
  		blink_led();
		}
	}
}
/*---------------------------------------------------------------------------*/
static void
MScallback(void *ptr)
{
	if(strcmp((char*)(ptr+(i-1)*20*2*71),"STOP")>0){
  	printf("%s\n",(char*)(ptr+(i-1)*20*2*71));
  	rn2483_send_cmd((char*)(ptr+(i-1)*20*2*71), (char*)(ptr+(i-1)*20*2*71+71));
  	i++;
  	ctimer_reset(&timer);
	}
	else{
  	i=0;
  	blink_led();
	}
}
/*---------------------------------------------------------------------------*/
void blink_led()
{
  //clock_init();
  leds_on(LEDS_BLUE);
  //clock_delay(50);
  leds_off(LEDS_BLUE);
}
/*---------------------------------------------------------------------------*/

void join_abp(char *devaddr, char *nwkskey,char *appskey)
{
  char radio_enum[20][2][71];
  (void)strcpy(radio_enum[0][0], "mac set devaddr");
  (void)strcpy(radio_enum[0][1], devaddr);

  (void)strcpy(radio_enum[1][0], "mac set nwkskey");
  (void)strcpy(radio_enum[1][1], nwkskey);

  (void)strcpy(radio_enum[2][0], "mac set appskey");
  (void)strcpy(radio_enum[2][1], appskey);

  (void)strcpy(radio_enum[3][0], "mac join abp");

  (void)strcpy(radio_enum[4][0], "STOP");
  ctimer_set(&timer, CLOCK_SECOND/20, MScallback, radio_enum);
}
/*---------------------------------------------------------------------------*/

void join_otaa(char *deveui, char *appeui,char *appkey)
{

  char radio_enum[20][2][71];
  (void)strcpy(radio_enum[0][0], "mac set deveui");
  (void)strcpy(radio_enum[0][1], deveui);

  (void)strcpy(radio_enum[1][0], "mac set appeui");
  (void)strcpy(radio_enum[1][1], appeui);

  (void)strcpy(radio_enum[2][0], "mac set appkey");
  (void)strcpy(radio_enum[2][1], appkey);

  (void)strcpy(radio_enum[3][0], "mac join otaa");

  (void)strcpy(radio_enum[4][0], "STOP");
  ctimer_set(&timer, CLOCK_SECOND/20, MScallback, radio_enum);
}
/*---------------------------------------------------------------------------*/
void vset_settings(const char *setting, const char *first, va_list argp)
{
	//size_t len;
  int callback=0;
	int drrange=0;
	int tel=0;
	//char *retbuf;
	char *p;
	char radio_enum[20][2][71];//because real enum too long for printing
	if(!strcmp(setting,"mac set")||!strcmp(setting,"mac set ch")){callback=1;}
	else{callback=0;}
	if(first == NULL){printf("error: missing argument\n");}
	//len = strlen(first)+strlen(setting);

	/*while((p = va_arg(argp, char*)) != NULL){
		len += strlen(p);
	}*/
	//va_end(argp); // reset ptr to first element
	//va_start(argp, first);// initialize arguments to store all arguments
	p = va_arg(argp, char*);
	//if(!strcmp(setting,"mac set ch ")){p =va_arg(argp, char*);}
	p = va_arg(argp, char*);// to avoid first 2

	while((p = va_arg(argp, char*)) != NULL){
		if(!strcmp(p,"drrange")){drrange=1;}
		else{drrange=0;}
		//retbuf = malloc(len+1);// 0/ character
		if(radio_enum[tel][0] == NULL){printf("error: missing argument\n");}
    (void)strcpy(radio_enum[tel][0], setting);
		if(tel==0)
      (void)strcpy(radio_enum[tel][1], first);
		else{
			(void)strcpy(radio_enum[tel][1], p);
			p = va_arg(argp, char*);
		}
		if(!strcmp(radio_enum[tel][0], "mac set rx2") || !strcmp(setting, "mac set ch")){
			(void)strcpy(radio_enum[tel][1], p);
			(void)strcat(radio_enum[tel][1], " ");
			(void)strcat(radio_enum[tel][1], va_arg(argp, char*));
			//printf("%s\n",retbuf);
		}
		else
      (void)strcpy(radio_enum[tel][1], p);
		if(drrange==1){
  		(void)strcpy(radio_enum[tel][1], va_arg(argp, char*));
  		//printf("%s\n",retbuf);
		}
		//(void)strcat(retbuf,"\r\n");
		//printf("%s",retbuf);
		//strcpy(radio_enum[tel],retbuf);
		//printf("%s",radio_enum[0]);
		//free(retbuf);
		tel++;
	}
	//va_end(argp);
	strcpy(radio_enum[tel][0],"STOP");
	if(callback==0){ctimer_set(&timer, CLOCK_SECOND/20, RScallback,radio_enum);}
	else{ctimer_set(&timer, CLOCK_SECOND/20, MScallback,radio_enum);}
}
/*---------------------------------------------------------------------------*/
void vget_settings(const char *setting, const char *first, va_list argp)
{
	//size_t len;
	int callback=0;
	int tel=0;
	//char *retbuf;
	char *p;
	char radio_enum[20][2][71];//because real enum too long for printing
	if(!strcmp(setting,"mac get ")){callback=1;}
	else{callback=0;}
	if(first == NULL){printf("error: missing argument\n");}
	//len = strlen(first)+strlen(setting);

	/*while((p = va_arg(argp, char*)) != NULL){
		len += strlen(p);
	}*/
	//va_end(argp); // reset ptr to first element
	//va_start(argp, first);// initialize arguments to store all arguments
	p=va_arg(argp, char*);
	while((p = va_arg(argp, char*)) != NULL){
		//retbuf = malloc(len+1);// 0/ character
		if(radio_enum[tel][0] == NULL){printf("error: missing argument\n");}
		(void)strcpy(radio_enum[tel][0], setting);
		if(tel == 0)
			(void)strcpy(radio_enum[tel][1], first);
		else
			(void)strcpy(radio_enum[tel][1], p);

		if(!strcmp(radio_enum[tel][0],"mac get rx2"))
      (void)strcpy(radio_enum[tel][1], va_arg(argp, char*));
		//(void)strcat(retbuf, "\r\n");
		//strcpy(radio_enum[tel],retbuf);
		//free(retbuf);
		tel++;
	}
	//va_end(argp);
	strcpy(radio_enum[tel][0],"STOP");
	if(callback==0){ctimer_set(&timer, CLOCK_SECOND/20, RScallback,radio_enum);}
	else{ctimer_set(&timer, CLOCK_SECOND/20, MScallback,radio_enum);}
}
/*---------------------------------------------------------------------------*/
void Ldelay()
{
 rtimer_arch_init();
 rtimer_clock_t time =rtimer_arch_now();
 while(RTIMER_CLOCK_LT(RTIMER_NOW(), time + INTER_PACKET_INTERVAL)) { }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(rn2483_process,ev,data)
{
  int len;
  PROCESS_BEGIN();
  printf("rn2483_process: started\n");
  GPIO_SET_OUTPUT(GPIO_D_BASE,GPIO_PIN_MASK(3));
  GPIO_SET_PIN(GPIO_D_BASE,GPIO_PIN_MASK(3));
  //char* rxdata;
  //uart_init(0);
  while(1) {
    PROCESS_YIELD_UNTIL(ev == lora_payload_message);
    packetbuf_clear();
    strcpy(rxbuf_data,(char *)data);
    //printf("process received: %s\n", (char *)data);



    len=rn2483_radio_read(packetbuf_dataptr(),PACKETBUF_SIZE);
    printf("received message: %s\n", (char *)data);

    if(len > 0) {
      packetbuf_set_datalen(len);

      NETSTACK_MAC.input();
    }
  }
  PROCESS_END();
}
