/*
 * Copyright (c) 2005, Swedish Institute of Computer Science
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */
#include "rn2483-uart.h"
#include "rn2483-config.h"
#include <string.h> /* for memcpy() */
#include <stdlib.h>
#include <stdbool.h>

#include "os/lib/ringbuf.h"
#include "dev/uart.h"
#include "dev/serial-line.h"

#include "sys/log.h"
#define LOG_MODULE "RN2483"
#define LOG_LEVEL LOG_LEVEL_ERR

#ifdef LORA_RECEPTION_CONF_BUFSIZE
#define BUFSIZE LORA_RECEPTION_CONF_BUFSIZE
#else /* LORA_RECEPTION_CONF_BUFSIZE */
#define BUFSIZE 64
#endif /* LORA_RECEPTION_CONF_BUFSIZE */

#if (BUFSIZE & (BUFSIZE - 1)) != 0
#error LORA_RECEPTION_CONF_BUFSIZE must be a power of two (i.e., 1, 2, 4, 8, 16, 32, 64, ...).
#error Change LORA_RECEPTION_CONF_BUFSIZE in contiki-conf.h.
#endif

#define IGNORE_CHAR(c) (c == 0x0d)
#define END 0x0a

static struct ringbuf rxbuf;
static uint8_t rxbuf_data[BUFSIZE];
static struct ringbuf cmdbuf;
static uint8_t cmdbuf_data[BUFSIZE];
static int lf = 0x0A;
static int cr = 0x0D;
bool strstartswith(const char *pre, const char *str);
void* getMsg(char* uart_msg);
PROCESS(lora_reception_process, "LoRa reception driver");

int rn2483_is_transmitting = 0;

process_event_t lora_payload_message;
process_event_t rn2483_event_message;

/*---------------------------------------------------------------------------*/
int
lora_input_byte(unsigned char c)
{
  static uint8_t overflow = 0; /* Buffer overflow: ignore until END */

  if(IGNORE_CHAR(c)) {
    return 0;
  }

  if(!overflow) {
    /* Add character */
    if(ringbuf_put(&rxbuf, c) == 0) {
      /* Buffer overflow: ignore the rest of the line */
      overflow = 1;
    }
  } else {
    /* Buffer overflowed:
     * Only (try to) add terminator characters, otherwise skip */
    if(c == END && ringbuf_put(&rxbuf, c) != 0) {
      overflow = 0;
    }
  }

  /* Wake up consumer process */
  process_poll(&lora_reception_process);
  return 1;
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(lora_reception_process, ev, data)
{
  //static char buf[BUFSIZE];
  //static char cmd[BUFSIZE];
  //static int ptr;

  PROCESS_BEGIN();
  printf("reception process started\n");
  uart_set_input(1,serial_line_input_byte);
  lora_payload_message = process_alloc_event();
  rn2483_event_message = process_alloc_event();

  //ptr = 0;
  void *rdata;

  while(1) {
    // Fill application buffer until newline or empty
    /*int c = ringbuf_get(&rxbuf);

    if(c == -1) {
      // Buffer empty, wait for poll
      PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);
    } else {
      if(c != END) {
        if(ptr < BUFSIZE-1) {
          buf[ptr++] = (uint8_t)c;
        } else {
          // Ignore character (wait for EOL)
        }
      } else {
        // Terminate
        buf[ptr++] = (uint8_t)'\0';

        if(!strstartswith("radio_rx",buf[0]))
        {
          ptr = 0;
          c = ringbuf_get(&cmdbuf);
          while(c != END)
          {
            buf[1][ptr++] = c;
          }
          buf[1][ptr++] = '\0';
          if RX_CONTINUOUS_MODE
            rn2483_send_cmd("radio rx", "0");
        }

        if( strcmp(buf[0],"invalid_param"))
        {
          LOG_ERR("Invalid parameter in command \"%s\"", buf[1]);
        }
        else if (strcmp(buf[0],"radio_err"))
        {
          if (strstartswith("radio rx", buf[1]))
          {
            LOG_WARN("Reception timed out or unsuccessful");
          }
          else if (strstartswith("radio tx", buf[1]))
          {
            LOG_WARN("Transmission was interrupted by WDT");
          }
        }
        else if (strcmp(buf[0],"busy"))
        {
          LOG_WARN("Tranceiver was busy while executing command \"%s\"", buf[1]);
        }

        // Broadcast event
        process_post(PROCESS_BROADCAST, lora_event_message, buf);
        printf("received over uart: %s\n",buf);

        // Wait until all processes have handled the lora event
        if(PROCESS_ERR_OK ==
          process_post(PROCESS_CURRENT(), PROCESS_EVENT_CONTINUE, NULL)) {
          PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_CONTINUE);
        }
        ptr = 0;
      }
    }*/
    PROCESS_YIELD();
    if(ev == serial_line_event_message)
    {
      rdata=data;
      //lora_input_byte(rdata);
      if (strstartswith("radio_rx ",(char *)rdata))
      {
        //strcpy(rxbuf_data,(char *)rdata[8]);
        if (RX_CONTINUOUS_MODE)
          rn2483_send_cmd("radio rx", "0");

        process_post(PROCESS_BROADCAST, lora_payload_message, rdata+8);
      }
      //process_post(PROCESS_BROADCAST, lora_event_message, rdata);
      if (rn2483_is_transmitting == 1 && (strstartswith("radio_tx_ok",(char *)rdata) 
          || strstartswith("radio_err",(char *)rdata)))
      {
        rn2483_is_transmitting = 0;
      }
      process_post(PROCESS_BROADCAST, rn2483_event_message, rdata);
      printf("uart data received: %s\n", (char *)rdata);
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
void
lora_reception_init(void)
{
  ringbuf_init(&rxbuf, rxbuf_data, sizeof(rxbuf_data));
  ringbuf_init(&cmdbuf, cmdbuf_data, sizeof(cmdbuf_data));
  process_start(&lora_reception_process, NULL);
}
/*---------------------------------------------------------------------------*/
/*void
rn2483_send(char *c)
{
int i=0;
while(c[i] != lf)// if a newline character is detected, stop sending after newline
{
  uart1_writeb(c[i++]);
}
uart1_writeb(lf);
}*/
/*---------------------------------------------------------------------------*/
void
rn2483_send_cmd(char *command, char *arg)
{
  int i = 0;
  char* scmd;
  scmd = malloc(sizeof(command)+sizeof(arg)+3);
  if (strcmp(command," ")!=0)
  {
    strcpy(scmd,command);
    /*while(command[i] != '\0')
    {
      printf("%c",command[i]);
      uart_write_byte(1, command[i++]);
      ringbuf_put(&cmdbuf,command[i++]);
    }*/
  }

  if (strcmp(command," ")!=0 && strcmp(arg," ")!=0)
  {
    strcat(scmd, " ");
    /*printf("-");
    uart_write_byte(1, 0x20); //space
    ringbuf_put(&cmdbuf,0x20);*/
  }

  if (strcmp(arg," ")!=0)
  {
    if (strcmp(command," ")==0)
      strcpy(scmd,arg);
    else
      strcat(scmd,arg);
    /*i = 0;
    while(c[i] != '\0')
    {
      printf("%c",c[i]);
      uart_write_byte(1, c[i++]);
      ringbuf_put(&cmdbuf,command[i++]);
    }*/
  }
  strcat(scmd,"\r\n");
  //printf("scmd: %s", scmd);
  /*uart_write_byte(1, cr);
  ringbuf_put(&cmdbuf,cr);
  uart_write_byte(1, lf);
  ringbuf_put(&cmdbuf,lf);*/


  while(scmd[i] != lf)// if a newline character is detected, stop sending after newline
{
  if(scmd[i] != cr)
    ringbuf_put(&cmdbuf,scmd[i]);
  uart_write_byte(1, scmd[i++]);
}
uart_write_byte(1, lf);
printf("Sending cmd: %s",scmd);
free(scmd);
}

bool strstartswith(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
}

/*void* getMsg(void* uart_msg)
{
  void* msg = 0;
  int i = 7;
  while(*uart_msg != cr)
  {
    *(msg+i) = *(uart_msg+i);
    i++;
  }

  return msg;
}*/
